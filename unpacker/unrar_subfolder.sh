#!/bin/bash
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")

for i in $(find $(pwd) -name '*.rar')
 do
  cd $(dirname $i)
  unrar x -o- $i
done

IFS=$SAVEIFS
