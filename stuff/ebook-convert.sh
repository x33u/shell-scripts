#!/bin/bash
# this thing will convert pdf's to epub's
# ebook-convert is shipped with "calibre"

## == create output dir
if [ ! -d converted ]; then
  mkdir converted
fi

## == get all pdf names
for NOVELLE in *.pdf

## == convert all given names
do
  ebook-convert \
    $NOVELLE \
    converted/${NOVELLE%????}.epub \
    --enable-heuristics
done
