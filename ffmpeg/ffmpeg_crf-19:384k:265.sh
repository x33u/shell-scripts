for i in *.mkv;
  do name=`echo $i | cut -d'.' -f1`;
  echo $name;
  ffmpeg -i "$i" -c:v libx265 -preset superfast -map_chapters -1 -crf 19 -c:a libfdk_aac -b:a 384k "${name}x265.mkv";
done
