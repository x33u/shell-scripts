#!/bin/sh

## == verbose output
set -x

## == export variables
export USER=cc1p
export HOST=10.0.0.12
export DST=/mnt/drive/backup/rootbox
export SERVER=sftp:$USER@$HOST:$DST
#export RESTIC_PASSWORD=<passwd>
export SRC=/home/cc1p
export TMP=$SRC/restic-backup
export EXC="
--exclude=**/.* \
--exclude=**/backups/systems \
--exclude=**/code/appimage \
--exclude=**/docs/TMP \
--exclude=**/dwnld \
--exclude=**/tmp \
--exclude=**/TMP \
--exclude=**/vmd \
--exclude-larger-than 8G \
--exclude-caches \
"

## == check if $TMP exist, if not then create
[ ! -d "$TMP" ] && mkdir -p $TMP && \
 rsync -r $SRC/.config $TMP && \
 rsync -r $SRC/.nano $TMP && \
 rsync -r $SRC/.ssh $TMP && \
 rsync -r $SRC/.var $TMP && \
 rsync -r $SRC/.themes $TMP && \
 rsync -r $SRC/.gnupg $TMP && \
 rsync -r $SRC/.zshrc $TMP && \
 rsync -r $SRC/.tmux.conf $TMP && \
 rsync -r $SRC/.nanorc $TMP && \
 rsync -r $SRC/.gitconfig $TMP

## == do the work
restic -r $SERVER backup $SRC $EXC 2>/dev/null

## == clean the place
if [ $? == 0 ]
then
  echo -e "\e[0;32m>>>> BACKUP COMPLETE <<<<\e[0m" \
  && [ -d "$TMP" ] && rm -rf $TMP
else
  echo -e "\e[1;31m>>>> BACKUP FAILED ! <<<<\e[0m" \
  >&2
fi

## == close
exit
