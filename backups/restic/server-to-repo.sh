#!/bin/sh

## == verbose output
set -x

## == export variables restic
export REPO=<target_repository>
export RESTIC_PASSWORD=<password>
export SRC=<source_path>
export EXC="
--exclude-caches \
"

## == do the work
/usr/bin/restic -r $REPO backup $SRC $EXC 2>/dev/null



## == export variables rsync
export SRCE=/mnt/drive/backup/restic/hidrive
export USER=<username>
export HOST=<host_address>
export PATH=<target_host_path>
export DEST=<target_folder>

## == do the work
/usr/bin/rsync -rvltDv -e '/usr/bin/ssh' $SRCE  $USER@$HOST:$PATH$DEST


## == clean the place
if [ $? == 0 ]
then
  echo -e "\e[0;32m>>>> BACKUP COMPLETE <<<<\e[0m" \

else
  echo -e "\e[1;31m>>>> BACKUP FAILED ! <<<<\e[0m" \
  >&2
fi

## == close
exit
