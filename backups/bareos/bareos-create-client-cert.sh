## print text
echo "type in hostname:"

## read the hostname variable
read HOSTNAME

## each client needs a keypair we have to generate the keyfile
openssl genrsa -aes256 -out $HOSTNAME-fd.key 4096

## from the keyfile we generate the client certificate
openssl req -new -key $HOSTNAME-fd.key \
  -x509 -out $HOSTNAME-fd.cert  \
  -subj "/C=DE/ST=Sachsen-Anhalt/L=Magdeburg/O=x33u/OU=IT/CN=$HOSTNAME.x33u.org/emailAddress=webmaster@x33u.org" \
  -days 3650

## create a PEM file for filedaemon encryption
openssl rsa -in $HOSTNAME-fd.key -out $HOSTNAME-fd-clear.key
cat $HOSTNAME-fd-clear.key $HOSTNAME-fd.cert > $HOSTNAME-fd.pem

## delete unneeded files
rm $HOSTNAME-fd-clear.key $HOSTNAME-fd.cert $HOSTNAME-fd.key

## create host folder
mkdir $HOSTNAME

## change permisions
chmod 640 $HOSTNAME-fd.pem

## move PEM file to host folder
mv $HOSTNAME-fd.pem $HOSTNAME/.
