#!/bin/bash
# backspin backup script
#set -x

# set vars
USER=golenz
TMP_DIR=./TMP_BACKUP
SERVER=w.x.y.z
SERVER_PATH=/mnt/ssd/backup/<device>/
SSH_USER=<username>
SSH_PORT=<ssh_port>
SSH_KEY=/home/<username>/.ssh/id_rsa
RSYNC_PATH=/usr/bin/openrsync

# make temp dir
mkdir -p $(TMP_DIR)/${USER}

# adb pull
adb pull sdcard/ $(TMP_DIR)/${USER}

# rsync tmp dir
rsync -rav \
      --rsh='ssh -p '"${SSH_PORT}"' -i '"${SSH_KEY}"'' \
      --rsync-path=${RSYNC_PATH} \
      --exclude={"Android","DCIM/.thumbnails","WhatsApp","TWRP"} \
        ${TMP_DIR}/${USER} ${SSH_USER}@${SERVER}:${SERVER_PATH} 2>/dev/null

## error-code
if [ $? == 0 ]
then
     echo "\n" \
  && echo -e "\e[0;32m#########################\e[0m" \
  && echo -e "\e[0;32m>>>> BACKUP COMPLETE <<<<\e[0m" \
  && echo -e "\e[0;32m#########################\e[0m" \
  &&echo "\n"
else
     echo "\n" \
  && echo -e "\e[1;31m#########################\e[0m" \
  && echo -e "\e[1;31m>>>> BACKUP FAILED ! <<<<\e[0m" \
  && echo -e "\e[1;31m#########################\e[0m" \
  &&echo "\n" >&2
fi

## exit
exit
