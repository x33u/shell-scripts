#!/bin/sh

## == verbose output
#set -x

export SRC=<source_path>
export USER=<username>
export HOST=<host_address>
export PATH=<host_path>
export DEST=<target_folder>

/usr/bin/rsync -rvltDv -e '/usr/bin/ssh' $SRC  $USER@$HOST:$PATH$DEST
